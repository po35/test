window.onload = function() {
    var element = document.getElementsByClassName('element')[0];
    //element = document.querySelector('.element');---другой спрособ.
    var offsetX = 0;
    var offsetY = 0;

    element.addEventListener('mousedown', (event)=> {
       // console.log(event);    
        offsetX = event.offsetX;
        offsetY = event.offsetY;    
        document.addEventListener('mousemove', mouseMoveHandle);
    });

    document.addEventListener('mouseup', () => {
        document.removeEventListener('mousemove', mouseMoveHandle);
    });

    function mouseMoveHandle(event) {
       element.style.left = event.pageX - offsetX + 'px';
       element.style.top = event.pageY - offsetY + 'px';
    }
}